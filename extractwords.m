clear all;

fid = fopen('cmudict.0.7a.txt', 'rb');
fidw = fopen('cmudict.0.7aword.txt', 'wb');
skipchars = {':', ';', '?', '.', '-', ')', '#', '(', ',', '/', '''', '"', '%', '&', '!', '{', '}'};
j = 1;
Word = [];
while ~feof(fid)
    X = fgetl(fid);    
    
    if ismember(X(1), skipchars)
        fprintf('[%d] skipped %s\n', j, X);
    else        
        Xtmp = strtok(X);
        if length(Xtmp) <= 2 ||~ismember(Xtmp(end-2:end), {'(1)', '(2)'});            
            fprintf(fidw, '%s\n', Xtmp);
            fprintf('%s,', Xtmp)
            Word{j, 1} = Xtmp;
            j = j + 1;        
        end
    end
end
fclose(fid);



fclose(fidw);




