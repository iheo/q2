import java.util.LinkedList;
import java.util.List;

public class MyTrie {
	private TrieNode root;	
	
	public MyTrie() {
		root = new TrieNode(' ');
	}

	// insertion to a Trie
	public void insert(String strWord) {
		
		// skip if the strWord is already exists
		// only for the case when the user wants to insert a new word
		if(isWordExist(strWord)) {
			System.out.println("Word already exists");
			return;
		}
		
		TrieNode current = root;
		char ch;
		for(int i = 0; i < strWord.length(); i++) {
			ch = strWord.charAt(i);			
			
			TrieNode node = current.getChildNode(ch);	// move to child node
			
			if(node == null) {	// if new element (word) create new node and append to child
				node = new TrieNode(ch);
				current.childList.add(node);	
				current = current.getChildNode(ch);
			} else {
				current = node;
			}			
		}
		current.isEndOfWord = true;
	}
	
	public boolean isWordExist(String strWord) {
		char ch;
		TrieNode current = root;
		for(int i = 0; i < strWord.length(); i++) {
			ch = strWord.charAt(i);
			if(current.getChildNode(ch) == null)
				return false;
			else
				current = current.getChildNode(ch);
		}
		if(current.isEndOfWord)
			return true;		
		return false;
	}
	
	// retrieve words recursively
	public List<String> getWordsRecur(TrieNode node, String prefix) {
		
		// do the recursion until there is no more child nodes
		if(node.childList.isEmpty() == true) {
			List<String> list = new LinkedList<String>();
			list.add(prefix + node.getElement());
			return list;
		} 
		else {
			List<String> list = new LinkedList<String>();
			if(node.isEndOfWord) {
				list.add(prefix + node.getElement());
			}
						
			for(TrieNode eachChild : node.childList) {
				list.addAll(getWordsRecur(eachChild, prefix + node.getElement()));	// add one char of each child to the next list
			}
			return list;
		}		
	}
	
	// main function for autoComplete
	public List<String> autoComplete(String prefix) {
		
		// when the prefix does not exist
		if(prefix == null || prefix.isEmpty()) {
			System.out.println("No Such prefix exists in dictionary!\n");
			return null;
		}
		prefix = prefix.toUpperCase();	// for simplicity
		
		TrieNode current = root;
		boolean flag = false;
		char ch;		

		for(int i = 0; i < prefix.length(); i++) {
			
			// compare each character in prefix
			ch = prefix.charAt(i);		
			
			if(current.childList.isEmpty() == false) {				
				// look for eachChild in all children linked to the current node
				for(TrieNode eachChild : current.childList)	{
					
					// if there exists a matching child==ch then update the current node and repeat
					if(eachChild.getElement() == Character.toUpperCase(ch))	{
						current = eachChild;
						flag=true;	// one matching
						break;
					}
					flag = false;	// no element was matched to eachChild -- may be repeated characters
				}
				
			}
			else {
				flag = false;	// when childList is empty, then it is done.
				break;
			}
		}
		if(flag) {
			List<String> matches = getWordsRecur(current, prefix.substring(0, prefix.length()-1));
			return matches;
		}
		return null;
	}	
}
