import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TrieTest {
	
	private static final String FNAME = "src/cmudict.0.7aword.txt";
	private static MyTrie trieNode;
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		// insert all words in dictionary
		Scanner scan = new Scanner(System.in);
		trieNode = new MyTrie();
		
		/*  construct Trie from a given dictionary file
		 * 
		 */ 
		Scanner inFile = new Scanner(new File(FNAME));
		
		List<String> temps = new ArrayList<String>();
		
		while(inFile.hasNext()) {
			temps.add(inFile.next());
		}
		inFile.close();
		
		String[] dictWords = temps.toArray(new String[0]);
		
		for(int i = 0; i < dictWords.length; i++) {
			trieNode.insert(dictWords[i]);	// insert words to dictwords
		}
		
		/* 
		 * Given a Trie, prints autocomplete starting with 'prefix'
		 */
		char ch;
		String prefix;
		
		List<String> mylist;	// autocompleted list
		
		do{
			System.out.println("Please type the prefix of a word\n");
			prefix = scan.next();
			mylist = trieNode.autoComplete(prefix);
			
			if(mylist == null) {
				System.out.println("No word with the prefix was found");
			}
			else {
				System.out.println("Suggestions:");
				for(String str : mylist)
					System.out.println(str);
			}
			
			
			System.out.println("\n Continue? (y)\n");
			ch = scan.next().charAt(0);
		} while(ch == 'Y' || ch == 'y');
		
		System.out.println("Thank you!");
	}

}
